package com.adel.services;

import com.adel.domain.User;

/**
 * Created by adelessam on 10/13/2016.
 */
public interface UserService {
    User save(User user);
     User getCurentUser();

}
