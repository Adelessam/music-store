package com.adel.services;

import com.adel.domain.Music;

import java.util.List;

/**
 * Created by adelessam on 10/12/2016.
 */
public interface MusicService {
    List<Music> listAllMusics();

    Music getMusicById(Integer id);

    Music saveOrUpdateMusic(Music music);

    void deleteMusic(int id);
}
