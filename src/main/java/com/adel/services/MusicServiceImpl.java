package com.adel.services;

import com.adel.domain.Music;
import com.adel.repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by adelessam on 10/12/2016.
 */
@Service
public class MusicServiceImpl implements MusicService {

    @Autowired
    MusicRepository musicRepository;

    @Autowired
    UserService userService;

    @Override
    public List<Music> listAllMusics() {
        return musicRepository.findByUser_Id(userService.getCurentUser().getId());
    }

    @Override
    public Music getMusicById(Integer id) {
        return musicRepository.findOne(id);
    }


    @Override
    public Music saveOrUpdateMusic(Music music) {
        music.setUser(userService.getCurentUser());
        return musicRepository.save(music);
    }

    @Override
    public void deleteMusic(int id) {
        musicRepository.delete(id);
    }
}
