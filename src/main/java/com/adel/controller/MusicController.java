package com.adel.controller;

import com.adel.domain.Music;
import com.adel.services.MusicService;
import com.adel.services.UserService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by adelessam on 10/12/2016.
 */
@Controller
public class MusicController {
    private MusicService musicService;

    @Autowired
    UserService userService;

    @Autowired
    public void setMusicService(MusicService musicService) {
        this.musicService = musicService;
    }

    @RequestMapping("/musics")
    public String listMusics(Model model) {
        model.addAttribute("musics", musicService.listAllMusics());
        return "musics";
    }

    @RequestMapping("/music/{id}")
    public String getMusic(@PathVariable int id, Model model) {
        model.addAttribute("music", musicService.getMusicById(id));
        return "music";

    }

    @RequestMapping("music/edit/{id}")
    public String editMusic(@PathVariable int id, Model model) {
        model.addAttribute("music", musicService.getMusicById(id));
        return "musicform";
    }

    @RequestMapping("/music/new")
    public String newMusic(Model model) {
        model.addAttribute("music", new Music());
        return "musicform";

    }

    @RequestMapping(value = "/music/new", method = RequestMethod.POST)
    public String saveOrUpdateMusic(@Valid @ModelAttribute("music") Music music, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "musicform";
        }

        try {
            music.setImageurl(Store(music.getImage()));
        } catch (IOException e) {
        }
        try {
            music.setMusicurl(Store(music.getMusic()));
        } catch (IOException e) {
        }

        Music savedMusic = musicService.saveOrUpdateMusic(music);
        return "redirect:/music/" + savedMusic.getId();
    }


    @RequestMapping("/music/delete/{id}")
    public String deleteMusic(@PathVariable int id) {
        musicService.deleteMusic(id);
        return "redirect:/musics";
    }


    private String Store(MultipartFile file) throws IOException {
        String path ;
        if (!file.isEmpty()) {
            String sp = File.separator;
            path = sp + "home" + sp + "vcap" + sp + "app" + sp + "BOOT-INF" + sp + "classes" + sp + "static" + sp + file.getOriginalFilename();
            Files.copy(file.getInputStream(), Paths.get(path));
        }
        return file.getOriginalFilename();
    }
}


///mvn clean install -DskipTests=true
// cf push -p gg  arpu-musicstore