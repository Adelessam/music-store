package com.adel.domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by adelessam on 10/13/2016.
 */
@Entity
public class User {
    @Id
    @GeneratedValue
    private Integer id;
    @NotBlank
    @Size(min = 5, max = 20)
    private String name;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    @Size(min = 6,max = 60)
    private String password;
    @NotBlank
    @Size(min = 11,max = 11)
    private String phone;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private List<Music> musics;

    public User(String name, String email, String password, String phone) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public User setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public User setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public List<Music> getMusics() {
        return musics;
    }

    public User setMusics(List<Music> musics) {
        this.musics = musics;
        return this;
    }
}
