package com.adel.domain;

import com.adel.validator.ContentType;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Created by adelessam on 10/12/2016.
 */
@Entity
public class Music {
    @Id
    @GeneratedValue
    private Integer id;
    @Size(min = 3,max = 20)
    @NotBlank
    private String singername;
    @Size(min = 3,max = 20)
    @NotBlank
    private String songname;
    @Size(min = 3,max = 20)
    @NotBlank
    private String albumname;
    private String imageurl;
    @Transient
    @ContentType(value = "image/jpeg",message = "image content type should be image/jpeg")
    private MultipartFile image;
    private String musicurl;
    @Transient
    @ContentType(value = "audio/mp3",message = "audio content type should be audio/mp3")
    private MultipartFile music;
    @ManyToOne
    @JoinColumn()
    private User user;
    public Music() {
    }

    public Music(String singername, String songname, String albumname) {
        this.singername = singername;
        this.songname = songname;
        this.albumname = albumname;
    }

    public Integer getId() {return id;}

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSingername() {
        return singername;
    }

    public void setSingername(String singername) {
        this.singername = singername;
    }

    public String getSongname() {
        return songname;
    }

    public void setSongname(String songname) {
        this.songname = songname;
    }

    public String getAlbumname() {
        return albumname;
    }

    public void setAlbumname(String albumname) {
        this.albumname = albumname;
    }

    public String getImageurl() {
        return imageurl;
    }

    public Music setImageurl(String imageurl) {
        this.imageurl = imageurl;
        return this;
    }

    public MultipartFile getImage() {
        return image;
    }

    public Music setImage(MultipartFile image) {
        this.image = image;
        return this;
    }

    public String getMusicurl() {
        return musicurl;
    }

    public Music setMusicurl(String musicurl) {
        this.musicurl = musicurl;
        return this;
    }

    public MultipartFile getMusic() {
        return music;
    }

    public Music setMusic(MultipartFile music) {
        this.music = music;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Music setUser(User user) {
        this.user = user;
        return this;
    }

}
