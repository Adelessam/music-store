package com.adel.config;

import com.adel.domain.User;
import com.adel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by adelessam on 10/13/2016.
 */
@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public PasswordEncoder encoder(){return new BCryptPasswordEncoder();}

    @Autowired
    UserRepository userRepository;

    @Bean
    public UserDetailsService userDetailsService(){
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
                User byEmail = userRepository.findByEmail(email);
                if(byEmail!=null){
                    return new org.springframework.security.core.userdetails.User(byEmail.getEmail(),byEmail.getPassword(), AuthorityUtils.createAuthorityList("ROLE_USER"));
                }else
                throw  new UsernameNotFoundException("Not Valid User ..!");
            }
        };

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(encoder());
    }


    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
//                .antMatchers("/musics/**").authenticated()
                .antMatchers("/music*/**").authenticated()
                .antMatchers("/music/**").authenticated()
                .antMatchers("/register").permitAll()
                .and().formLogin().defaultSuccessUrl("/musics")
                .loginPage("/login")
                .permitAll().and().csrf().disable();
    }
}
