package com.adel.repository;

import com.adel.domain.Music;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by adelessam on 10/13/2016.
 */
public interface MusicRepository extends JpaRepository<Music,Integer> {
    List<Music> findByUser_Id(Integer id);

}
