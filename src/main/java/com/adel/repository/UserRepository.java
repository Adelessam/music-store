package com.adel.repository;

import com.adel.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by adelessam on 10/13/2016.
 */
public interface UserRepository extends JpaRepository<User,Integer> {
 User findByEmail(String email);
}
